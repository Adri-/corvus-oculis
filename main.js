require('dotenv').config();

const { Client, Intents } = require('discord.js');
const discordclient = new Client({ intents: [Intents.FLAGS.GUILDS] });
const fs = require('fs');
const chokidar = require('chokidar');
const {Mutex, Semaphore, withTimeout} = require('async-mutex');

const mutex = new Mutex();

let saveData = function() {
    fs.writeFileSync(process.env.DATAFILE, JSON.stringify(data));
}

console.log("Starting program");
let datafilecontents;
try {
    datafilecontents = fs.readFileSync(process.env.DATAFILE);
} catch(e) {}
let data;
try {
    data = JSON.parse(datafilecontents);
} catch(e) {
    data = {
        "towns": {},
        "players": {}
    };
    saveData();
}
let messages = JSON.parse(fs.readFileSync("messages.json"));
let guild;
let channel;

let handleNewCitizenRole = async function(event) {
    let townProps = data.towns[event.town];
    if(!townProps) {
        await guild.roles.create({
            "name": event.town,
            "color": "#b55a14"
        }).then(role => {
            data.towns[event.town] = {
                "id": role.id,
                "citizens": []
            };
            saveData();
            townProps = data.towns[event.town];
        }).catch(console.log);
    }
    if(townProps) {
        townProps.citizens.push(event.player);
        saveData();
        if(data.players.hasOwnProperty(event.player)) {
            await guild.members.fetch(data.players[event.player].discordid).then(async member => {
                await member.roles.add(townProps.id);
            })
        }
    }
}

let messageFromEvent = function(event) {
    let message = "";
    if(messages.hasOwnProperty(event.type)) {
        message = messages[event.type];

        message = message.replaceAll("{town.name}", event.town);
        message = message.replaceAll("{town.days}", event.towndays);
        message = message.replaceAll("{town.mention}", "<@&"+data.towns[event.town].id+">");
        message = message.replaceAll("{citizen.name}", event.citizen);
    }
    return message;        
}

let addDiscord = function(event) {
    discordclient.users.fetch(event.discordid).then(user => {
        user.createDM().then(dm => {
            dm.send("Vous êtes désormais sous ma surveillance, "+event.username+".").catch(console.log);

            data.players[event.userid] = {
                "name": event.username,
                "discordid": event.discordid
            };
            saveData();
        }).catch(console.log);
    }).catch(console.log);;
}

let endTown = function(event) {
    guild.roles.fetch(data.towns[event.town].id).then(role => {
        role.delete().catch(console.log);
        delete data.towns[event.town];
        saveData();
    })
}

let eventHandler = async function(path) {
    mutex.acquire()
        .then(async (release) => {
            try {
                let event = JSON.parse(fs.readFileSync(path));

                try {
                    if(event.type == "link") {
                        addDiscord(event);
                    } else if(event.type == "end") {
                        endTown(event);
                    } else {
                        await handleNewCitizenRole(event);
                    }
                    let message = messageFromEvent(event);
                    if(!!message) {
                        channel.send(message);
                    }
                } catch(e) {
                    console.log("Impossible d'exécuter l'event : ");
                    console.log(event);
                    console.log(e);
                }
            } catch(e) {
                console.log("Impossible de lire l'event "+path);
                console.log(e);
            } finally {
                fs.rmSync(path);
                release();
            }
        })
        .catch(console.log);
}

console.log("Discord bot starting...");
discordclient.login(process.env.TOKEN).then(async () => {
    await discordclient.guilds.fetch(process.env.GUILDID).then((g) => guild = g);
    await discordclient.channels.fetch(process.env.CHANID).then((c) => channel = c);
    
    console.log("Enabling watcher...");
    var watcher = chokidar.watch('./input', {ignored: /^\./, persistent: true});

    console.log("Listening for events");
    watcher.on('add', eventHandler)
      .on('error', function(error) {console.error('Error:', error);})
});